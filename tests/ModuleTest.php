<?php

namespace DoctrineRestModuleTest;

class ModuleTest extends \PHPUnit_Framework_TestCase
{

    public function testGetConfig()
    {
        $this->assertTrue(file_exists(__DIR__ . '/../config/module.config.php'));

        $module = new \DoctrineRestModule\Module();
        $config = $module->getConfig();

        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('doctrine-rest', $config);
    }

    public function testGetAutoloaderConfig()
    {
        $module = new \DoctrineRestModule\Module();
        $config = $module->getAutoloaderConfig();

        $this->assertTrue(is_array($config));
    }

}
