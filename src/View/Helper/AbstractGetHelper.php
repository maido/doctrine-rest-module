<?php

namespace DoctrineRestModule\View\Helper;

use Zend\View\Helper\AbstractHelper;
use DoctrineRestModule\Rest\ResourceInterface;

abstract class AbstractGetHelper extends AbstractHelper
{

    /**
     * @var \Zend\Filter\FilterInterface
     */
    protected $fieldNameFilter;

    public function __invoke(ResourceInterface $resource = null)
    {
        if ($resource === null) {
            return $this;
        }

        return $this->render($resource);
    }

    abstract public function render(ResourceInterface $resource);
}
