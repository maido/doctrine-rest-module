<?php

namespace DoctrineRestModule\View\Helper;

use DoctrineRestModule\Rest\ResourceInterface;
use DoctrineRestModule\Rest\AbstractResource;

class Resource extends AbstractGetHelper
{

    public function render(ResourceInterface $resource)
    {
        return $this->table($resource);
    }

    public function table(AbstractResource $resource)
    {
        $fields = $resource->getResourceFields();
        $data = $resource->toArray();

        $html = '<table class="table table-hover">';

        foreach ($data as $name => $value) {
            if (!in_array($name, $fields)) {
                continue;
            }

            $name = $this->getView()->title($name);

            if (is_bool($value)) {
                $value = $value ? 'Yes' : 'No';
            }

            $html .= sprintf('<tr><th>%s</th><td>%s</td></tr>', $name, $value);
        }

        $html .= '</table>';

        return $html;
    }

    public function preview(ResourceInterface $resource)
    {
        $entity = $resource->toObject();

        if ($entity instanceof \Application\Entity\File) {
            if ($entity->getWidth() && $entity->getHeight()) {
                $params = [
                    'filesystem' => $entity->getFilesystem(),
                    'name' => $entity->getName(),
                ];
                $src = $this->getView()->url('file', $params);
                return sprintf('<a href="%1$s" target="_blank" title="Click to open full size"><img src="%1$s" alt="%2$s" class="img-thumbnail" style="max-width: 240px;"></a>', $src, $entity->getName());
            } elseif ($entity->getLength()) {
                // @todo video or audio
            } elseif (substr($entity->getMimetype(), 0, 5) === 'text/') {
                // @todo text
            }
        }


        return '';
    }

}
