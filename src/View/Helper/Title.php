<?php

namespace DoctrineRestModule\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Filter\FilterInterface;
use Zend\Filter\Word\CamelCaseToSeparator;
use Zend\Filter\Word\UnderscoreToSeparator;
use Zend\Filter\Word\DashToSeparator;

class Title extends AbstractHelper
{

    protected $defaultFilters = [
        CamelCaseToSeparator::class,
        UnderscoreToSeparator::class,
        DashToSeparator::class,
    ];

    public function __invoke($title, array $filters = null)
    {
        if (!is_scalar($title)) {
            throw new \InvalidArgumentException('Argument "title" is expected to be a scalar value.');
        }

        if ($filters === null) {
            $filters = $this->defaultFilters;
        }

        foreach ($filters as $filterClass) {
            $filter = new $filterClass();

            if (!$filter instanceof FilterInterface) {
                throw new \InvalidArgumentException('Argument "filters" must only contain instances of ' . FilterInterface::class);
            }

            $title = $filter->filter($title);
        }

        return ucwords($title);
    }

}
