<?php

namespace DoctrineRestModule\View\Helper;

use DoctrineRestModule\Rest\ResourceInterface;
use DoctrineRestModule\Rest\AbstractCollection;

class Collection extends AbstractGetHelper
{

    public function render(ResourceInterface $resource)
    {
        if (!$resource instanceof AbstractCollection) {
            $message = sprintf('Resource must be an instance of "%s".', AbstractCollection::class);
            throw new \InvalidArgumentException($message);
        }

        $paginator = $this->paginator($resource);

        $html = $paginator ? sprintf('<p>%s</p>', $paginator) : '';
        $html .= $this->table($resource);

        return $html;
    }

    public function paginator(AbstractCollection $collection)
    {
        if (!$collection->toObject() instanceof \Zend\Paginator\Paginator) {
            return '';
        }

        $pages = $collection->toObject()->getPages();
        $query = $collection->getOptions();

        // pages
        $current = $pages->current;
        $first = $pages->first;
        $previous = isset($pages->previous) ? $pages->previous : $pages->first;
        $next = isset($pages->next) ? $pages->next : $pages->last;
        $last = $pages->last;

        $view = $this->getView();

        $html = '<div class="btn-group" role="group">';

        $query['page'] = $first;
        $link = $view->url(null, [], ['query' => $query], true);
        $class = $first === $current ? ' disabled' : '';
        $html .= sprintf('<a href="%s" class="btn btn-default%s"><span class="glyphicon glyphicon-fast-backward"></span></a>', $link, $class);

        $query['page'] = $previous;
        $link = $view->url(null, [], ['query' => $query], true);
        $class = $previous === $current ? ' disabled' : '';
        $html .= sprintf('<a href="%s" class="btn btn-default%s"><span class="glyphicon glyphicon-backward"></span></a>', $link, $class);

        $query['page'] = $next;
        $link = $view->url(null, [], ['query' => $query], true);
        $class = $next === $current ? ' disabled' : '';
        $html .= sprintf('<a href="%s" class="btn btn-default%s"><span class="glyphicon glyphicon-forward"></span></a>', $link, $class);

        $query['page'] = $last;
        $link = $view->url(null, [], ['query' => $query], true);
        $class = $last === $current ? ' disabled' : '';
        $html .= sprintf('<a href="%s" class="btn btn-default%s"><span class="glyphicon glyphicon-fast-forward"></span></a>', $link, $class);

        // items
        $first = $pages->firstItemNumber;
        $last = $pages->lastItemNumber;
        $total = $pages->totalItemCount;

        $html .= sprintf('<spane class="btn disabled">%d to %d of %d</span>', $first, $last, $total);

        $html .= '</div>';

        return $html;
    }

    public function table(AbstractCollection $collection)
    {
        $data = [];

        $identifier = $collection->getResourceIdentifier();
        $titlefier = $collection->getResourceTitlefier();

        foreach ($collection->toObject() as $entity) {
            $resource = $collection->getResourceFromEntity($entity);

//            if ($collection instanceof HalResourceInterface && $resource instanceof HalResourceInterface) {
//                $resource->setUrlPlugin($collection->getUrlPlugin());
//            }

            $item = $resource->toArray();
            $link = $this->getLink($resource);
            $row = [];

            if (isset($item[$identifier])) {
                $key = $this->getView()->title($identifier);
                $value = $titlefier === $identifier ? sprintf('<strong><a href="%s">%s</a></strong>', $link, $item[$identifier]) : $item[$identifier];
                $row[$key] = $value;
            }

            if (isset($item[$titlefier]) && $titlefier != $identifier) {
                $key = ucwords(str_replace('_', ' ', $titlefier));
                $value = sprintf('<strong><a href="%s">%s</a></strong>', $link, $item[$titlefier]);
                $row[$key] = $value;
            }

            if (isset($item['created'])) {
                $value = date('j M Y H:i', strtotime($item['created']));
                $row['Created'] = $value;
            }

            if (isset($item['updated'])) {
                $value = date('j M Y H:i', strtotime($item['updated']));
                $row['Updated'] = $value;
            }

            if (count($row)) {
                $data[] = $row;
            }
        }

        if (count($data) === 0) {
            return '';
        }

        $html = '<table class="table table-hover table-responsive">';
        $html .= '<thead><tr>';

        foreach (current($data) as $header => $value) {
            $html .= sprintf('<th>%s</th>', $header);
        }

        $html .= '</tr></thead>';
        $html .= '<tbody>';

        foreach ($data as $row) {
            $html .= '<tr>';

            foreach ($row as $field) {
                $html .= sprintf('<td>%s</td>', $field);
            }

            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }

    /**
     * Get link
     * 
     * @param ResourceInterface $resource
     * @return string
     */
    protected function getLink(ResourceInterface $resource)
    {
//        if ($resource instanceof HalResourceInterface) {
//            return $resource->getLinks()->getLink('self')->getHref();
//        }

        $identifier = $resource->getResourceIdentifier();
        $data = $resource->toArray();
        $id = $data[$identifier];

        return $this->getView()->url(null, ['id' => $id], ['force_canonical' => true], true);
    }

    public function options(AbstractCollection $collection)
    {
        /* @var $paginator \Zend\Paginator\Paginator */
        $paginator = $collection->toObject();
        $pages = $paginator->getPages();

        $html = '<form action="" method="GET">';
        $html .= '<div class="row">';

        // page
        $html .= '<div class="col-sm-2">';
        $html .= '<label class="control-label">Page';
        $html .= '<select name="page" class="form-control">';
        for ($value = 1; $value <= $pages->pageCount; $value++) {
            $selected = $value == $pages->current ? ' selected="selected"' : '';
            $html .= sprintf('<option value="%1$d"%2$s>%1$d</option>', $value, $selected);
        }
        $html .= '</select>';
        $html .= '</label>';
        $html .= '</div>';

        // items
        $html .= '<div class="col-sm-2">';
        $html .= '<label class="control-label">Items';
        $html .= '<select name="items" class="form-control">';
        foreach ([10, 25, 50, 100, 200] as $value) {
            $selected = $value == $paginator->getItemCountPerPage() ? ' selected="selected"' : '';
            $html .= sprintf('<option value="%1$d"%2$s>%1$d</option>', $value, $selected);
        }
        $html .= '</select>';
        $html .= '</label>';
        $html .= '</div>';

        $html .= '</div>';

        $html .= '<p><button type="submit" class="btn btn-primary">GET</button></p>';

        $html .= '</form>';

        return $html;
    }

}
