<?php

namespace DoctrineRestModule;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    /**
     * On bootstrap
     * 
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        $eventManager = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_ROUTE, [$this, 'authorize'], -100);
    }

    /**
     * Get config
     * 
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * Get autoloader config
     * 
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }

    /**
     * Authorize
     * 
     * @param MvcEvent $e
     * @return void|Response
     */
    public function authorize(MvcEvent $e)
    {
        $config = $e->getApplication()->getServiceManager()->get('config');

        $requireBearer = isset($config['doctrine-rest']['require_bearer']) && $config['doctrine-rest']['require_bearer'];
        if (!$requireBearer) {
            // token not required
            return;
        }

        $protectedRoute = isset($config['doctrine-rest']['protected_route']) ? $config['doctrine-rest']['protected_route'] : 'doctrine-rest';

        $protect = false;
        $matched = '';
        foreach (explode('/', $e->getRouteMatch()->getMatchedRouteName()) as $part) {
            $matched = trim("$matched/$part", '/');
            if (true === $protect = ($matched === $protectedRoute)) {
                break;
            }
        }

        if (!$protect) {
            // matched route doesn't require protection
            return;
        }

        $bearerToken = isset($config['doctrine-rest']['bearer_token']) ? $config['doctrine-rest']['bearer_token'] : null;

        $headers = $e->getRequest()->getHeaders();
        if ($headers->has('Authorization')) {
            $header = $headers->get('Authorization');
            if ($header->getFieldValue() === "Bearer $bearerToken") {
                // token is ok
                return;
            }
        }

        // invalid token
        $response = $e->getResponse();
        $response->setStatusCode(401);

        return $response;
    }

}
