<?php

namespace DoctrineRestModule\Repository;

use Zend\Cache\Storage\Adapter\AbstractAdapter;

interface Cacheable
{

    /**
     * Set cache
     *
     * @param AbstractAdapter $adapter
     */
    public function setCache(AbstractAdapter $adapter = null);

    /**
     * @return AbstractAdapter
     */
    public function getCache();
}
