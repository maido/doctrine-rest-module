<?php

namespace DoctrineRestModule\Exception;

class RuntimeException extends \RuntimeException implements ApplicationExceptionInterface
{
    
}
