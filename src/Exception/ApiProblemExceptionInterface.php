<?php

namespace DoctrineRestModule\Exception;

interface ApiProblemExceptionInterface
{

    /**
     * Get additional details
     * 
     * @return array
     */
    public function getAdditionalDetails();

    /**
     * Get described by
     * 
     * @return string
     */
    public function getDescribedBy();

    /**
     * Get title
     * 
     * @return string
     */
    public function getTitle();
}
