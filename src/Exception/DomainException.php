<?php

namespace DoctrineRestModule\Exception;

class DomainException extends \DomainException implements ApplicationExceptionInterface, ApiProblemExceptionInterface
{

    /**
     * @var string
     */
    protected $describedBy;

    /**
     * @var array
     */
    protected $details = [];

    /**
     * @var string
     */
    protected $title;

    /**
     * Set additional details
     *
     * @param array $details
     * @return \DoctrineRestModule\Exception\DomainException
     */
    public function setAdditionalDetails(array $details)
    {
        $this->details = $details;
        return $this;
    }

    /**
     * Set described by
     *
     * @param string $uri
     * @return \DoctrineRestModule\Exception\DomainException
     */
    public function setDescribedBy($uri)
    {
        $this->describedBy = (string) $uri;
        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return \DoctrineRestModule\Exception\DomainException
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * Get additional details#
     *
     * @return array
     */
    public function getAdditionalDetails()
    {
        return $this->details;
    }

    /**
     * Get described by
     *
     * @return string
     */
    public function getDescribedBy()
    {
        return $this->describedBy;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

}
