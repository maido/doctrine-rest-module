<?php

namespace DoctrineRestModule\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ApplicationExceptionInterface
{
    
}
