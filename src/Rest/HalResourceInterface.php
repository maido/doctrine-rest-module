<?php

namespace DoctrineRestModule\Rest;

use Zend\Mvc\Controller\Plugin\Url;

interface HalResourceInterface
{

    /**
     * 
     * @param Url $plugin
     * @return \DoctrineRestModule\Rest\HalResourceInterface
     */
    public function setUrlPlugin(Url $plugin);

    /**
     * @return \Zend\Mvc\Controller\Plugin\Url|null
     */
    public function getUrlPlugin();
}
