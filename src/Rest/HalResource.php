<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception;
use DoctrineFileModule\Entity\FileInterface;

class HalResource extends AbstractResource implements HalResourceInterface
{

    use HalResourceAwareTrait;

    /**
     * Get
     * 
     * @param string $id
     * @return \DoctrineRestModule\Rest\ArrayResource
     * @throws Exception\DomainException
     */
    public function get($id = null)
    {
        $repository = $this->getEntityManager()
                ->getRepository($this->config['class']);

        if ($repository instanceof \DoctrineRestModule\Repository\Cacheable) {
            $repository->setCache($this->getCacheAdapter());
        }

        $entity = $repository->find($id);

        if (!$entity) {
            $message = sprintf('Unable to find a resource with id "%s".', $id);
            throw new Exception\DomainException($message);
        }

        $this->data = $entity;

        return $this;
    }

    /**
     * To array
     * 
     * @return array
     */
    public function toArray()
    {
        if ($this->isCacheable()) {
            $key = $this->getCacheKey();
            if ($this->getCacheAdapter()->hasItem($key)) {
                return $this->getCacheAdapter()->getItem($key);
            }
        }

        $fields = $this->getFields();
        $embeds = $this->getEmbeds();

        $data = $this->getHydrator()->extract($this->data);

        foreach ($data as $name => $value) {
            if (!in_array($name, $fields) && !isset($embeds[$name])) {
                unset($data[$name]);
                continue;
            }

            if ($value === null) {
                unset($data[$name]);
            } elseif ($value instanceof \DateTime) {
                $data[$name] = $value->format('c');
            } elseif (is_bool($value)) {
                $data[$name] = $value;
            } elseif (isset($embeds[$name])) {
                $resource = $this->prepareEmbed($embeds[$name], $name, $value);
                $data['_embedded'][$resource->getName()] = $resource->toArray();
                unset($data[$name]);
            } elseif (is_resource($value)) {
                $data[$name] = stream_get_contents($value);
            } elseif (!is_scalar($value)) {
                unset($data[$name]);
            }
        }

        $data['_links'] = $this->getLinks();

        if ($this->isCacheable()) {
            $key = $this->getCacheKey();
            $this->getCacheAdapter()->setItem($key, $data);
        }

        return $data;
    }

    public function getLinks()
    {
        $query = $this->options;
        $url = $this->getUrlPlugin()->fromRoute(null, [
            'name' => $this->getName(),
            'id' => $this->data->getId()
                ], [
            'query' => $query,
            'force_canonical' => true
                ], true);

        $links = [
            'self' => ['href' => $url],
        ];

        if (is_subclass_of($this->config['class'], FileInterface::class) && $files = $this->restService->getFileService()) {
            $links['file']['href'] = $files->getUrl($this->data);
        }

        return $links;
    }

}
