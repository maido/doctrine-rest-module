<?php

namespace DoctrineRestModule\Rest;

interface ResourceInterface
{

    /**
     * Return data as object
     *
     * @return object
     */
    public function toObject();

    /**
     * Return an array representation of data
     * 
     * @return array
     */
    public function toArray();
}
