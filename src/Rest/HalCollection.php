<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception;
use Zend\Paginator\Paginator;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ArrayCollection;

class HalCollection extends AbstractCollection implements HalResourceInterface
{

    use HalResourceAwareTrait;

    /**
     * Get
     *
     * @param string|null $id
     * @return \DoctrineRestModule\Rest\ArrayCollection
     */
    public function get($id = null)
    {
        $repository = $this->getEntityManager()
                ->getRepository($this->config['class']);

        $page = $this->getPage();
        $items = $this->getItems();

        if (!$this->isRandom()) {
            $criteria = Criteria::create()
                    ->orderBy($this->getOrderings());

            if ($where = $this->getWhere()) {
                $criteria->where($where);
            }
        } else {
            $criteria = null;
            $data = $repository->findAll();
            shuffle($data);
            $slice = array_slice($data, 0, $items);
            $repository = new ArrayCollection($slice);
        }

        $paginator = $this->paginate($repository, $page, $items, $criteria);
        $this->data = $paginator;

        return $this;
    }

    /**
     * To array
     *
     * @return array
     * @throws Exception\RuntimeException
     */
    public function toArray()
    {
        if ($this->isCacheable()) {
            $key = $this->getCacheKey();
            if ($this->getCacheAdapter()->hasItem($key)) {
                return $this->getCacheAdapter()->getItem($key);
            }
        }

        if (!$this->data instanceof Paginator) {
            throw new Exception\RuntimeException('No data requested; run "get" before.');
        }

        $resource = $this->restService->getResource($this->config['class'], $this->options);
        $resource->setUrlPlugin($this->getUrlPlugin());

        $collection = [];

        foreach ($this->data as $entity) {
            $resource->setData($entity);
            $collection[] = $resource->toArray();
        }

        $pages = $this->data->getPages();

        $data = [
            'current_page' => $pages->current,
            'first_page' => $pages->first,
            'previous_page' => isset($pages->previous) ? $pages->previous : $pages->first,
            'next_page' => isset($pages->next) ? $pages->next : $pages->last,
            'last_page' => $pages->last,
            'total_items' => $pages->totalItemCount,
            'items_per_page' => $pages->itemCountPerPage,
            'current_items' => $pages->currentItemCount,
        ];

        $data['_embedded'] = $collection;
        $data['_links'] = $this->getLinks();

        if ($this->isCacheable()) {
            $key = $this->getCacheKey();
            $this->getCacheAdapter()->setItem($key, $data);
        }

        return $data;
    }

    public function getLinks()
    {
        $links = [];
        $query = $this->options;
        $pages = $this->data->getPages();

        $current = $pages->current;
        $first = $pages->first;
        $previous = isset($pages->previous) ? $pages->previous : $pages->first;
        $next = isset($pages->next) ? $pages->next : $pages->last;
        $last = $pages->last;

        $options = [];
        $options['self'] = $current;
        $options['first'] = $first;
        if ($current > $first) {
            $options['previous'] = $previous;
        }
        if ($current < $last) {
            $options['next'] = $next;
        }
        $options['last'] = $last;

        foreach ($options as $name => $page) {
            $query['page'] = $page;
            $url = $this->getUrlPlugin()->fromRoute(null, [
                'name' => $this->getName(),
                    ], [
                'query' => $query,
                'force_canonical' => true
                    ], true);

            $links[$name] = ['href' => $url];
        }

        return $links;
    }

}
