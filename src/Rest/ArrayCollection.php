<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception;
use Zend\Paginator\Paginator;
use Doctrine\Common\Collections\Criteria;

class ArrayCollection extends AbstractCollection
{

    /**
     * Get
     * 
     * @param string|null $id
     * @return \DoctrineRestModule\Rest\ArrayCollection
     */
    public function get($id = null)
    {
        $repository = $this->getEntityManager()
                ->getRepository($this->config['class']);

        $page = $this->getPage();
        $items = $this->getItems();

        if (!$this->isRandom()) {
            $criteria = Criteria::create()
                    ->orderBy($this->getOrderings());

            if ($where = $this->getWhere()) {
                $criteria->where($where);
            }
        } else {
            $criteria = null;
            $data = $repository->findAll();
            shuffle($data);
            $slice = array_slice($data, 0, $items);
            $repository = new \Doctrine\Common\Collections\ArrayCollection($slice);
        }

        $paginator = $this->paginate($repository, $page, $items, $criteria);
        $this->data = $paginator;

        return $this;
    }

    /**
     * To array
     * 
     * @return array
     * @throws Exception\DomainException
     */
    public function toArray()
    {
        $resource = $this->restService->getResource($this->config['class'], $this->options);
        $data = [];

        foreach ($this->data as $entity) {
            $resource->setData($entity);
            $data[] = $resource->toArray();
        }

        return $data;
    }

}
