<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception;

class ArrayResource extends AbstractResource
{

    /**
     * Get
     * 
     * @param string $id
     * @return \DoctrineRestModule\Rest\ArrayResource
     * @throws Exception\DomainException
     */
    public function get($id = null)
    {
        $entity = $this->getEntityManager()
                ->getRepository($this->config['class'])
                ->find($id);

        if (!$entity) {
            $message = sprintf('Unable to find a resource with id "%s".', $id);
            throw new Exception\DomainException($message);
        }

        $this->data = $entity;

        return $this;
    }

    /**
     * To array
     * 
     * @return array
     */
    public function toArray()
    {
        $fields = $this->getFields();
        $embeds = $this->getEmbeds();

        $data = $this->getHydrator()->extract($this->data);

        foreach ($data as $name => $value) {
            if (!in_array($name, $fields) && !isset($embeds[$name])) {
                unset($data[$name]);
                continue;
            }

            if ($value instanceof \DateTime) {
                $data[$name] = $value->format('c');
            } elseif (is_bool($value)) {
                $data[$name] = $value;
            } elseif (isset($embeds[$name])) {
                $resource = $this->prepareEmbed($embeds[$name], $name, $value);
                $data[$name] = $resource->toArray();
            } elseif (!is_scalar($value)) {
                unset($data[$name]);
            }
        }

        return $data;
    }

}
