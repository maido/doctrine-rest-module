<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception\ApiProblemExceptionInterface;

class ApiProblem implements ResourceInterface
{

    const DESCRIBEDBY_URL = 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html';

    /**
     * @var array
     */
    protected $additionalDetails = [];

    /**
     * @var string
     */
    protected $describedBy = self::DESCRIBEDBY_URL;

    /**
     * @var string
     */
    protected $detail = '';

    /**
     * @var bool
     */
    protected $detailIncludesStackTrace = false;

    /**
     * @var int
     */
    protected $httpStatus;

    /**
     * @var array
     */
    protected $normalizedProperties = [
        'describedby' => 'describedBy',
        'httpstatus' => 'httpStatus',
        'title' => 'title',
        'detail' => 'detail',
    ];

    /**
     * @var array
     * @link https://tools.ietf.org/html/draft-nottingham-http-problem-02
     */
    protected $problemStatusTitles = [
        // CLIENT ERROR
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        // SERVER ERROR
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    ];

    /**
     * @var string
     */
    protected $title;

    /**
     * Class constructor
     *
     * @param int $httpStatus
     * @param string|Exception\ProblemExceptionInterface $detail
     * @param string $describedBy
     * @param string $title
     * @param array $additional
     */
    public function __construct($httpStatus, $detail, $describedBy = null, $title = null, array $additional = [])
    {
        if ($detail instanceof ApiProblemExceptionInterface) {
            if (null === $describedBy) {
                $describedBy = $detail->getDescribedBy();
            }

            if (null === $title) {
                $title = $detail->getTitle();
            }

            if (empty($additional)) {
                $additional = $detail->getAdditionalDetails();
            }
        }

        $this->httpStatus = $httpStatus;
        $this->detail = $detail;
        $this->title = $title;

        if (null !== $describedBy) {
            $this->describedBy = $describedBy;
        }

        $this->additionalDetails = $additional;
    }

    /**
     * Retrieve properties
     *
     * @param  string $name
     * @return mixed
     */
    public function __get($name)
    {
        $normalized = strtolower(str_replace(['_', '-'], '', $name));

        if (in_array($normalized, array_keys($this->normalizedProperties))) {
            $property = $this->normalizedProperties[$normalized];
            return $this->$property;
        }

        if (isset($this->additionalDetails[$name])) {
            return $this->additionalDetails[$name];
        }

        if (isset($this->additionalDetails[$normalized])) {
            return $this->additionalDetails[$normalized];
        }

        $message = sprintf('Invalid property name "%s".', $name);
        throw new Exception\InvalidArgumentException($message);
    }

    /**
     * To object
     *
     * @return \Application\Rest\ApiProblem
     */
    public function toObject()
    {
        return $this;
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray()
    {
        $problem = [
            'described_by' => $this->describedBy,
            'title' => $this->getTitle(),
            'http_status' => $this->getHttpStatus(),
            'detail' => $this->getDetail(),
        ];

        if ($this->additionalDetails) {
            $problem['additional_details'] = $this->additionalDetails;
        }

        return $problem;
    }

    /**
     * Set detail includes stack trace
     *
     * @param  bool $flag
     * @return ApiProblem
     */
    public function setDetailIncludesStackTrace($flag)
    {
        $this->detailIncludesStackTrace = (bool) $flag;
        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    protected function getDetail()
    {
        if ($this->detail instanceof \Exception) {
            return $this->createDetailFromException();
        }

        return $this->detail;
    }

    /**
     * Get HTTP status
     *
     * @return string
     */
    protected function getHttpStatus()
    {
        if ($this->httpStatus) {
            return $this->httpStatus;
        }

        if ($this->detail instanceof \Exception) {
            $this->httpStatus = $this->createStatusFromException();
        }

        return $this->httpStatus;
    }

    /**
     * Get title
     *
     * @return string
     */
    protected function getTitle()
    {
        if (null !== $this->title) {
            return $this->title;
        }

        if (null === $this->title && $this->describedBy == self::DESCRIBEDBY_URL && array_key_exists($this->getHttpStatus(), $this->problemStatusTitles)) {
            return $this->problemStatusTitles[$this->httpStatus];
        }

        if ($this->detail instanceof \Exception) {
            return get_class($this->detail);
        }

        if (null === $this->title) {
            return 'Untitled';
        }

        return $this->title;
    }

    /**
     * Create detail from exception.
     *
     * @return string
     */
    protected function createDetailFromException()
    {
        $e = $this->detail;

        if (!$this->detailIncludesStackTrace) {
            return $e->getMessage();
        }

        $message = '';

        while ($e instanceof \Exception) {
            $message .= $e->getMessage() . "\n";
            $message .= $e->getTraceAsString() . "\n";
            $e = $e->getPrevious();
        }

        return trim($message);
    }

    /**
     * Create status from exception.
     *
     * @return string
     */
    protected function createStatusFromException()
    {
        $e = $this->detail;
        $httpStatus = $e->getCode();

        if (!empty($httpStatus)) {
            return $httpStatus;
        }

        return 500;
    }

    /**
     * To Json
     *
     * @return string
     */
    public function toJson()
    {
        return Json::encode($this->toArray(), false, ['prettyPrint' => true]);
    }

}
