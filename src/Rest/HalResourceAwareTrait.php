<?php

namespace DoctrineRestModule\Rest;

use Zend\Mvc\Controller\Plugin\Url;

trait HalResourceAwareTrait
{

    /**
     * @var \Zend\Mvc\Controller\Plugin\Url
     */
    protected $plugin;

    /**
     * 
     * @param Url $plugin
     * @return \DoctrineRestModule\Rest\HalResourceInterface
     */
    public function setUrlPlugin(Url $plugin)
    {
        $this->plugin = $plugin;
        return $this;
    }

    /**
     * @return \Zend\Mvc\Controller\Plugin\Url|null
     */
    public function getUrlPlugin()
    {
        return $this->plugin;
    }

}
