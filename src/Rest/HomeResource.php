<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Service\Rest\RestServiceInterface;
use Zend\Json\Json;

class HomeResource implements ResourceInterface
{

    /**
     * @var \DoctrineRestModule\Service\Rest\RestServiceInterface
     */
    protected $restService;

    public function __construct(RestServiceInterface $restService)
    {
        $this->restService = $restService;
    }

    public function getTitle()
    {
        return isset($this->restService->getConfig()['title']) ? $this->restService->getConfig()['title'] : 'API';
    }

    public function getResources()
    {
        $resources = [];

        foreach ($this->restService->getResources() as $name => $config) {
            $resources[] = $this->restService->getResource($name, [], true);
        }

        return $resources;
    }

    public function toArray()
    {
        $data = [
            'title' => $this->getTitle(),
            'resources' => [],
        ];

        foreach ($this->getResources() as $resource) {
            $data['resources'][] = [
                'title' => $resource->getTitle(),
                'route' => $resource->getRoute(),
                'name' => $resource->getName(),
            ];
        }

        return $data;
    }

    public function toJson()
    {
        return Json::encode($this->toArray(), false, ['prettyPrint' => true]);
    }

    public function toObject()
    {
        return $this;
    }

}
