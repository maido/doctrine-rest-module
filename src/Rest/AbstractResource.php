<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception;
use DoctrineRestModule\Service\Rest\RestServiceInterface;
use Zend\Hydrator\HydratorInterface;
use Zend\Hydrator\NamingStrategy\NamingStrategyInterface;
use Zend\Hydrator\NamingStrategy\UnderscoreNamingStrategy;
use Zend\Json\Json;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\Common\Collections\Criteria;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;
use DoctrineModule\Paginator\Adapter\Selectable as SelectableAdapter;
use Zend\Paginator\Paginator;
use Zend\Stdlib\RequestInterface;

abstract class AbstractResource implements ResourceInterface
{

    use \DoctrineRestModule\Service\Rest\RestServiceAwareTrait;

    /**
     * @var \Zend\Hydrator\HydratorInterface
     */
    protected $hydrator;

    /**
     * @var \Zend\Hydrator\NamingStrategy\NamingStrategyInterface
     */
    protected $namingStrategy;

    /**
     * @var object
     */
    protected $data;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Zend\Stdlib\RequestInterface
     */
    protected $request;

    /**
     * Class constructor
     *
     * @param array $config
     * @param array $options
     * @param RestServiceInterface $service
     */
    public function __construct(array $config, array $options, RestServiceInterface $service, RequestInterface $request = null)
    {
        $this->setConfig($config);
        $this->setOptions($options);

        $this->restService = $service;
        $this->request = $request;
    }

    /**
     * Get entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->restService->getEntityManager();
    }

    /**
     * Set hydrator
     *
     * @param HydratorInterface $hydrator
     * @return \DoctrineRestModule\Rest\AbstractResource
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
        return $this;
    }

    /**
     * Get hydrator
     *
     * @return \Zend\Hydrator\HydratorInterface
     */
    public function getHydrator()
    {
        if ($this->hydrator === null) {
            $this->hydrator = new DoctrineObject($this->getEntityManager());
            $this->hydrator->setNamingStrategy($this->getNamingStrategy());
        }

        return $this->hydrator;
    }

    /**
     * Set naming strategy
     *
     * @param NamingStrategyInterface $namingStrategy
     * @return \DoctrineRestModule\Rest\AbstractResource
     */
    public function setNamingStrategy(NamingStrategyInterface $namingStrategy)
    {
        $this->namingStrategy = $namingStrategy;
        return $this;
    }

    /**
     * Get naming strategy
     *
     * @return \Zend\Hydrator\NamingStrategy\NamingStrategyInterface
     */
    public function getNamingStrategy()
    {
        if ($this->namingStrategy === null) {
            $this->namingStrategy = new UnderscoreNamingStrategy();
        }

        return $this->namingStrategy;
    }

    /**
     * Get cache adapter
     *
     * @return \Zend\Cache\Storage\Adapter\AbstractAdapter|null
     */
    public function getCacheAdapter()
    {
        if (isset($this->options['cache']) && !$this->options['cache']) {
            return null;
        }

        return $this->restService->getCacheAdapter();
    }

    /**
     * Get cache key
     *
     * @return string
     */
    public function getCacheKey()
    {
        if ($this->request && $uri = $this->request->getUri()) {
            $key = (string) $uri;
        } else {
            $key = uniqid();
        }

        return md5($key);
    }

    /**
     * Set data
     *
     * @param object|array $data
     * @return \DoctrineRestModule\Rest\AbstractResource
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Set config
     *
     * @param array $config
     * @return \DoctrineRestModule\Rest\AbstractResource
     */
    public function setConfig(array $config)
    {
        // @todo sanitize config
        $this->config = $config;
        return $this;
    }

    /**
     * Get config
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Get allowed options
     *
     * @return array
     */
    public function getAllowedOptions()
    {
        return ['page', 'items', 'fields', 'embeds', 'orderings', 'where', 'random', 'metadata', 'cache'];
    }

    /**
     * Set options
     *
     * @param array $options
     * @return \DoctrineRestModule\Rest\AbstractResource
     */
    public function setOptions(array $options)
    {
        $this->options = [];
        $allowed = $this->getAllowedOptions();

        foreach ($options as $name => $value) {
            if (in_array($name, $allowed)) {
                $this->options[$name] = $options[$name];
            }
        }

        return $this;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->config['resource_title'];
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->config['name'];
    }

    public function getRoute()
    {
        return $this->config['route'];
    }

    /**
     * Get resource fields
     *
     * @return array
     */
    public function getResourceFields()
    {
        $fields = [];
        $entityFields = $this->getEntityManager()
                ->getClassMetadata($this->config['class'])
                ->getFieldNames();

        foreach ($entityFields as $name) {
            $fields[] = $this->getNamingStrategy()->extract($name);
        }

        return $fields;
    }

    /**
     * Get resource identifier
     *
     * @return string
     * @throws Exception\DomainException
     */
    public function getResourceIdentifier()
    {
        $identifiers = $this->getEntityManager()
                ->getClassMetadata($this->config['class'])
                ->getIdentifierFieldNames();

        if (count($identifiers) > 1) {
            throw new Exception\DomainException('Composed keys are not supported.');
        }

        return array_shift($identifiers);
    }

    /**
     * Get resource titlefier
     *
     * @return string
     */
    public function getResourceTitlefier()
    {
        if (isset($this->config['titlefier']) && is_string($this->config['titlefier'])) {
            return $this->config['titlefier'];
        }

        $fields = $this->getResourceFields();
        if (in_array('name', $fields)) {
            return 'name';
        } elseif (in_array('title', $fields)) {
            return 'title';
        }

        return $this->getResourceIdentifier();
    }

    /**
     * Get fields
     *
     * @return array
     */
    public function getFields()
    {
        $resourceFields = $this->getResourceFields();

        if (!isset($this->options['fields']) || !is_array($this->options['fields'])) {
            return $resourceFields;
        }

        $fields = [$this->getResourceIdentifier()];

        foreach ($this->options['fields'] as $field) {
            if (is_string($field) && in_array($field, $resourceFields) && !in_array($field, $fields)) {
                $fields[] = $field;
            }
        }

        return $fields;
    }

    /**
     * Get resource embeds
     *
     * @return array
     */
    public function getResourceEmbeds()
    {
        $embeds = [];

        $metadata = $this->getEntityManager()
                ->getClassMetadata($this->config['class']);

        foreach ($metadata->getAssociationNames() as $name) {
            $entityClass = $metadata->getAssociationTargetClass($name);
            $name = $this->getNamingStrategy()->extract($name);
            $embeds[$name] = $entityClass;
        }

        return $embeds;
    }

    /**
     * Get embeds
     *
     * @return array
     */
    public function getEmbeds()
    {
        if (!isset($this->options['embeds']) || !is_array($this->options['embeds'])) {
            return [];
        }

        $resourceEmbeds = $this->getResourceEmbeds();
        $embeds = [];

        foreach ($this->options['embeds'] as $embed) {
            if (is_string($embed) && isset($resourceEmbeds[$embed]) && !in_array($embed, $embeds)) {
                $embeds[$embed] = $resourceEmbeds[$embed];
                continue;
            }

            foreach ($resourceEmbeds as $alias => $class) {
                $resource = $this->restService->getResource($class);
                if ($embed === $resource->getName()) {
                    $embeds[$alias] = $class;
                    break;
                }
            }
        }

        return $embeds;
    }

    /**
     * Is random
     *
     * @return boolean
     */
    public function isRandom()
    {
        if (!isset($this->options['random'])) {
            return false;
        }

        if (!is_array($this->options['random'])) {
            return (bool) $this->options['random'];
        }

        if (is_array($this->options['random']) && is_int(key($this->options['random']))) {
            return (bool) current($this->options['random']);
        }

        return false;
    }

    /**
     * Prepare embed
     *
     * @param string $entityClass
     * @param string $name
     * @param object $value
     * @return AbstractResource
     */
    protected function prepareEmbed($entityClass, $name, $value)
    {
        $options = [];

        foreach ($this->getAllowedOptions() as $option) {
            if (isset($this->options[$option][$name])) {
                $options[$option] = $this->options[$option][$name];
            }
        }

        if (array_key_exists('cache', $this->options)) {
            $options['cache'] = $this->options['cache'];
        }

        $isCollection = $value instanceof Selectable;

        /* @var $resource AbstractResource */
        $resource = $this->restService->getResource($entityClass, $options, $isCollection);

        if ($this instanceof HalResourceInterface) {
            $resource->setUrlPlugin($this->plugin);
        }

        if ($isCollection) {
            $page = $resource->getPage();
            $items = $resource->getItems();
            $orderings = $resource->getOrderings();

            if ($orderings) {
                $criteria = Criteria::create()->orderBy($orderings);
            } else {
                $criteria = null;
            }

            $data = $this->paginate($value, $page, $items, $criteria);
        } else {
            $data = $value;
        }

        return $resource->setData($data);
    }

    /**
     * Paginate collection
     *
     * @param EntityRepository|Collection $source
     * @param int $page
     * @param int $items
     * @param Criteria $criteria
     * @return Paginator
     */
    protected function paginate($source, $page, $items, Criteria $criteria = null)
    {
        if (!$source instanceof Selectable) {
            $type = is_object($source) ? get_class($source) : gettype($source);
            $message = sprintf('Argument "source" at "%s" must be an instance of "%s"; "%s" was given.', __METHOD__, Selectable::class, $type);
            throw new Exception\InvalidArgumentException($message);
        }

        if (method_exists($source, 'getMapping') && $source->getMapping()['type'] == 8) {
            // many-to-many needs to extract data before filter
            $collection = new \Doctrine\Common\Collections\ArrayCollection($source->toArray());
            if ($criteria === null) {
                $criteria = new Criteria();
            }
            $adapter = new CollectionAdapter($collection->matching($criteria));
        } else {
            $adapter = new SelectableAdapter($source, $criteria);
        }

        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($items);

        return $paginator;
    }

    /**
     * Return data as object
     *
     * @return object|array
     */
    public function toObject()
    {
        return $this->data;
    }

    /**
     * To JSON
     *
     * @return string
     */
    public function toJson()
    {
        return Json::encode($this->toArray(), false, ['prettyPrint' => true]);
    }

    abstract public function toArray();

    abstract public function get($id = null);

    /**
     *
     * @return boolean
     */
    public function isCacheable()
    {
        return $this->getCacheAdapter() && !$this->isRandom() && $this->request;
    }

    /**
     *
     * @return boolean
     */
    public function flushCache()
    {
        if ($this->getCacheAdapter()) {
            return $this->getCacheAdapter()->flush();
        }

        return false;
    }

    protected function validate($data, $includeIdentifier = true)
    {
        if (!$this->validateUniqueConstraints($data)) {
            $exception = new Exception\DomainException('A record with these values already exists.', 400);
            throw $exception;
        }

        /* @var $inputFilter \Zend\InputFilter\InputFilter */
        $inputFilter = $this->restService->getInputFilter($this->config['class'], $includeIdentifier);
        $inputFilter->setData($data);

        if ($this->request && $this->request->getMethod() === 'PATCH') {
            foreach ($inputFilter->getInputs() as $input) {
                $input->setRequired(false);
            }
        }

        $context = [];

        if (is_object($this->data) && method_exists($this->data, 'getId') && $id = $this->data->getId()) {
            $context['id'] = $id;
        }

        foreach ($this->getResourceEmbeds() as $name => $class) {
            $method = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            if (is_object($this->data) && method_exists($this->data, $method) && $value = $this->data->$method()) {
                $context[$name] = $value;
            }
        }

        if (!$inputFilter->isValid($context)) {
            $exception = new Exception\DomainException('Invalid data submitted; check additional details.', 400);
            $exception->setAdditionalDetails($inputFilter->getMessages());
            throw $exception;
        }

        $valid = $inputFilter->getValues();

        $embeds = $this->getResourceEmbeds();

        foreach ($data as $key => $embed) {
            if (is_array($embed) && isset($embeds[$key])) {
                foreach ($embed as $value) {
                    $valid[$key][] = $value;
                }
            }
        }

        return $valid;
    }

    protected function validateUniqueConstraints($data)
    {
        $metadata = $this->getEntityManager()->getClassMetadata($this->config['class']);
        if (!isset($metadata->table['uniqueConstraints'])) {
            return true;
        }

        $repository = $this->getEntityManager()->getRepository($this->config['class']);
        
        foreach ($metadata->table['uniqueConstraints'] as $columns) {
            $fields = [];
            foreach ($columns['columns'] as $column) {
                if (isset($metadata->fieldNames[$column])) {
                    $name = $column;
                } else {
                    if (substr($column, -3) !== '_id') {
                        throw new \RuntimeException('Custom names on unique constraints not supported.');
                    }
                    $name = substr($column, 0, -3);
                }
                $fields[] = $name;
            }

            $criteria = [];
            foreach ($fields as $name) {
                if (array_key_exists($name, $data)) {
                    $criteria[$name] = $data[$name];
                }
            }
            
            $duplicate = $repository->findOneBy($criteria);
            if ($duplicate) {
                return false;
            }
        }

        return true;
    }

    public function post($data)
    {
        $valid = $this->validate($data);

        $object = new $this->config['class'];
        $entity = $this->getHydrator()->hydrate($valid, $object);

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        $this->flushCache();

        $this->data = $entity;
    }

    public function put($data)
    {
        $valid = $this->validate($data, false);
        foreach ($valid as $key => $value) {
            if (!array_key_exists($key, $data)) {
                unset($valid[$key]);
            }
        }

        $this->getHydrator()->hydrate($valid, $this->data);

        $this->getEntityManager()->persist($this->data);
        $this->getEntityManager()->flush();
        $this->flushCache();
    }

    public function patch($data)
    {
        $valid = $this->validate($data, false);
        foreach ($valid as $key => $value) {
            if (!array_key_exists($key, $data)) {
                unset($valid[$key]);
            }
        }

        $this->getHydrator()->hydrate($valid, $this->data);

        $this->getEntityManager()->persist($this->data);
        $this->getEntityManager()->flush();
        $this->flushCache();
    }

    public function delete()
    {
        $this->getEntityManager()->remove($this->data);
        $this->getEntityManager()->flush();
        $this->flushCache();
    }

}
