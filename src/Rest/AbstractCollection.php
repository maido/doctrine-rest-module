<?php

namespace DoctrineRestModule\Rest;

use DoctrineRestModule\Exception;
use Doctrine\Common\Collections\Criteria;

abstract class AbstractCollection extends AbstractResource
{

    /**
     * @var array
     */
    protected static $methods = [
        'or' => 'orX',
        'and' => 'andX',
        'eq' => 'eq',
        'neq' => 'neq',
        'gt' => 'gt',
        'gte' => 'gte',
        'lt' => 'lt',
        'lte' => 'lte',
        'null' => 'isNull',
        'in' => 'in',
        'nin' => 'notIn',
        'like' => 'contains',
    ];

    /**
     * Get title
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->config['collection_title'];
    }

    /**
     * Get page
     *
     * @return int|null
     */
    public function getPage()
    {
        if (isset($this->options['page'][0]) && is_array($this->options['page']) && $this->options['page'][0] > 0) {
            return (int) $this->options['page'][0];
        } elseif (isset($this->options['page']) && $this->options['page'] > 0) {
            return (int) $this->options['page'];
        }

        return 1;
    }

    /**
     * Get items
     *
     * @return int|null
     */
    public function getItems()
    {
        if (isset($this->options['items'][0]) && is_array($this->options['items']) && $this->options['items'][0] > 0) {
            return (int) $this->options['items'][0];
        } elseif (isset($this->options['items']) && !is_array($this->options['items']) && $this->options['items'] > 0) {
            return (int) $this->options['items'];
        }

        $items = isset($this->config['default_items_per_page']) && $this->config['default_items_per_page'] > 0 ?
                (int) $this->config['default_items_per_page'] : 10;

        return $items;
    }

    /**
     * Get orderings
     * 
     * @return array
     */
    public function getOrderings()
    {
        if (!isset($this->options['orderings']) || !is_array($this->options['orderings'])) {
            return [];
        }

        $fields = $this->getResourceFields();
        $strategy = $this->getNamingStrategy();
        $orderings = [];

        foreach ($this->options['orderings'] as $name => $value) {
            if (!is_int($name)) {
                continue;
            }

            if (is_string($value) && in_array($value, $fields)) {
                $field = $value;
                $order = 'asc';
            } elseif (is_array($value) && !empty($value) && is_string(key($value)) && is_string(current($value))) {
                $name = key($value);
                $value = strtolower($value[$name]);
                if (!in_array($name, $fields) || !in_array($value, ['asc', 'desc'])) {
                    continue;
                }
                $field = $name;
                $order = $value;
            } else {
                continue;
            }

            $orderings[$strategy->hydrate($field)] = $order;
        }

        return $orderings;
    }

    /**
     * Get where
     * 
     * @return \Doctrine\Common\Collections\ExpressionBuilder|null
     */
    public function getWhere()
    {
        if (!isset($this->options['where']) || !is_array($this->options['where'])) {
            return null;
        }

        foreach ($this->options['where'] as $method => $arguments) {
            if (isset(self::$methods[$method])) {
                if (in_array($method, ['and', 'or'])) {
                    return $this->prepareCompositeExpression($method, $arguments);
                } else {
                    return $this->prepareComparison($method, $arguments);
                }
            }
        }

        return null;
    }

    /**
     * Prepare composite expression
     * 
     * @param string $method
     * @param array $arguments
     * @return \Doctrine\Common\Collections\ExpressionBuilder
     */
    protected function prepareCompositeExpression($method, $arguments)
    {
        if (!is_array($arguments)) {
            return null;
        }

        $args = [];

        foreach ($arguments as $argument) {
            if (is_array($argument)) {
                $next = key($argument);
                if (!isset(self::$methods[$next])) {
                    continue;
                }
                if (in_array($next, ['and, or'])) {
                    $args[] = $this->prepareCompositeExpression($next, current($argument));
                } else {
                    $args[] = $this->prepareComparison($next, current($argument));
                }
            }
        }

        return call_user_func_array([Criteria::expr(), self::$methods[$method]], $args);
    }

    /**
     * Prepare comparison
     * 
     * @param string $method
     * @param array|string $arguments
     * @return \Doctrine\Common\Collections\ExpressionBuilder
     * @throws Exception\DomainException
     */
    protected function prepareComparison($method, $arguments)
    {
        if (is_array($arguments)) {
            $field = key($arguments);
            $value = current($arguments);

            if (in_array($method, ['in', 'nin'])) {
                $value = explode(',', $value);
            }
        } elseif (is_string($arguments)) {
            $field = $arguments;
        } else {
            $type = is_object($arguments) ? get_class($arguments) : gettype($arguments);
            $message = sprintf('Argument "arguments" in "%s" is expected to be an array or a string; "%s" was given.', __METHOD__, $type);
            throw new Exception\DomainException($message);
        }

        $fields = $this->getResourceFields();

        if (!in_array($field, $fields)) {
            $message = sprintf('Unknow field "%s" in resource for "%s".', $field, $this->config['class']);
            throw new Exception\DomainException($message);
        }
        
        $mapping = $this->getFieldMetadata($field);
        if (in_array($mapping['type'], ['datetime', 'date'])) {
            $value = new \DateTime($value);
        }

        $method = self::$methods[$method];
        $field = $this->getNamingStrategy()->hydrate($field);

        if ($method === 'isNull') {
            return Criteria::expr()->$method($field);
        }
        
        return Criteria::expr()->$method($field, $value);
    }
    
    /**
     * 
     * @param string $field
     * @return array
     */
    public function getFieldMetadata($field)
    {
        $metadata = $this->getEntityManager()->getClassMetadata($this->config['class']);
        if (!isset($metadata->fieldNames[$field])) {
            return null;
        }
        
        return $metadata->fieldMappings[$metadata->fieldNames[$field]];
    }

    /**
     * Get resource from entity
     *
     * @param object $entity
     * @return AbstractResource
     */
    public function getResourceFromEntity($entity)
    {
        $name = get_class($entity);
        $resource = $this->restService->getResource($name, $this->options);
        $resource->setData($entity);

        if ($this instanceof HalResourceInterface) {
            $resource->setUrlPlugin($this->plugin);
        }

        return $resource;
    }

}
