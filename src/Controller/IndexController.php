<?php

namespace DoctrineRestModule\Controller;

class IndexController extends AbstractRestfulController
{

    public function getList()
    {
        $resource = new \DoctrineRestModule\Rest\HomeResource($this->restService);
        return $this->getViewModel($resource);
    }

}
