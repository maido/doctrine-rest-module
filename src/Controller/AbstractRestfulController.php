<?php

namespace DoctrineRestModule\Controller;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use DoctrineRestModule\Service\Rest\RestServiceInterface;
use DoctrineRestModule\Rest\ResourceInterface;
use DoctrineRestModule\Rest\ApiProblem;
use DoctrineRestModule\Exception\ApiProblemExceptionInterface;
use DoctrineRestModule\Rest\HalResourceInterface;
use Zend\Log\LoggerInterface;

abstract class AbstractRestfulController extends \Zend\Mvc\Controller\AbstractRestfulController
{

    use \DoctrineRestModule\Service\Rest\RestServiceAwareTrait;

    /**
     * @var array
     */
    protected $acceptCriteria = [
        JsonModel::class => [
            'application/hal+json',
            'application/json',
            'text/json',
        ],
        ViewModel::class => [
            'text/html',
            'application/xhtml+xml',
        ]
    ];

    /**
     * @var \Zend\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Class constructor
     *
     * @param RestServiceInterface $restService
     */
    public function __construct(RestServiceInterface $restService, LoggerInterface $logger)
    {
        $this->restService = $restService;
        $this->logger = $logger;
    }

    /**
     * Set accept criteria
     *
     * @param array $criteria
     * @return \Application\Rest\Resource
     */
    public function setAcceptCriteria(array $criteria)
    {
        $this->acceptCriteria = $criteria;
        return $this;
    }

    /**
     * Get accept criteria
     *
     * @return array
     */
    public function getAcceptCriteria()
    {
        return $this->acceptCriteria;
    }

    /**
     * Get view model
     *
     * @param array $data
     * @return \Zend\View\Model\ModelInterface
     */
    protected function getViewModel(ResourceInterface $resource)
    {
        $viewModel = $this->acceptableViewModelSelector($this->acceptCriteria);

        if ($resource instanceof ApiProblem) {
            $this->response->setStatusCode($resource->httpStatus);
        }

        if ($viewModel instanceof JsonModel) {
            $viewModel->setVariables($resource->toArray());
            return $viewModel;
        }

        if ($resource instanceof ApiProblem) {
            $template = 'error/api-problem';
            $viewModel->setTemplate($template);
        }

        $viewModel->setVariables([
            'resource' => $resource,
            'request' => $this->request,
            'response' => $this->response,
        ]);

        return $viewModel;
    }

    /**
     * Get resource
     *
     * @param bool $isPost
     * @return \DoctrineRestModule\Rest\ResourceInterface
     */
    protected function getResource($isPost = false)
    {
        $name = $this->params()->fromRoute('name');
        $options = $this->params()->fromQuery();
        $options['request'] = $this->getRequest();

        $isCollection = !$isPost && !$this->params()->fromRoute('id');

        try {
            $resource = $this->restService->getResource($name, $options, $isCollection);
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = 404;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        if ($resource instanceof HalResourceInterface) {
            $plugin = $this->getPluginManager()->get('Url');
            $resource->setUrlPlugin($plugin);
        }

        return $resource;
    }

    /**
     * Options
     *
     * @return \Zend\View\Model\ModelInterface
     */
    public function options()
    {
        $route = 'doctrine-rest/documentation';
        $link = $this->url()->fromRoute($route, [], ['force_canonical' => true], true);
        $this->response->getHeaders()->addHeaders([
            'Allowed: OPTIONS',
            'Link: ' . $link,
        ]);
    }

    /**
     * Get list
     *
     * @return \Zend\View\Model\ModelInterface
     */
    public function getList()
    {
        $resource = new ApiProblem(405, 'Method not implemented.');
        return $this->getViewModel($resource);
    }

    /**
     * Get
     *
     * @return \Zend\View\Model\ModelInterface
     */
    public function get($id)
    {
        $resource = new ApiProblem(405, 'Method not implemented.');
        return $this->getViewModel($resource);
    }

    public function replaceList($data)
    {
        $resource = new ApiProblem(405, 'Can\'t update many resources at once.');
        return $this->getViewModel($resource);
    }

    public function deleteList($data)
    {
        $resource = new ApiProblem(405, 'Can\'t delete many resources at once.');
        return $this->getViewModel($resource);
    }

    /**
     * Log an exception
     *
     * @param \Exception $e
     */
    protected function logException(\Exception $e)
    {
        $trace = $e->getTraceAsString();
        $i = 1;
        
        do {
            $messages[] = $i++ . ": " . $e->getMessage();
        } while ($e = $e->getPrevious());

        $log = "Exception:\n" . implode("\n", $messages);
        $log .= "\nTrace:\n" . $trace;

        $this->logger->err($log);
    }

}
