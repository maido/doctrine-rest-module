<?php

namespace DoctrineRestModule\Controller;

use DoctrineRestModule\Exception\ApiProblemExceptionInterface;
use DoctrineRestModule\Rest\ApiProblem;

class ResourceController extends AbstractRestfulController
{

    public function getList()
    {
        $resource = $this->getResource();

        if ($resource instanceof ApiProblem) {
            return $this->getViewModel($resource);
        }

        try {
            $resource->get();
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = 404;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        return $this->getViewModel($resource);
    }

    public function get($id)
    {
        $resource = $this->getResource();

        try {
            $resource->get($id);
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = 404;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        return $this->getViewModel($resource);
    }

    public function create($data)
    {
        $resource = $this->getResource(true);

        try {
            $resource->post($data);
            $this->response->setStatusCode(201);
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = 400;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        return $this->getViewModel($resource);
    }

    public function update($id, $data)
    {
        $resource = $this->getResource();

        try {
            $resource->get($id);
            $resource->put($data);
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = 404;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        return $this->getViewModel($resource);
    }

    public function patch($id, $data)
    {
        $resource = $this->getResource();

        try {
            $resource->get($id);
            $resource->patch($data);
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = $exception->getCode() ? : 400;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        return $this->getViewModel($resource);
    }

    public function delete($id)
    {
        $resource = $this->getResource();

        try {
            $resource->get($id);
            $resource->delete();
        } catch (ApiProblemExceptionInterface $exception) {
            $httpStatus = 404;
            $resource = new ApiProblem($httpStatus, $exception);
        } catch (\Exception $exception) {
            $this->logException($exception);
            $httpStatus = $exception->getCode() ? : 500;
            $resource = new ApiProblem($httpStatus, $exception->getMessage());
        }

        return $this->getViewModel($resource);
    }

}
