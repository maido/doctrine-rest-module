<?php

namespace DoctrineRestModule\Validator;

class StringLength extends \Zend\Validator\StringLength
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID   => "Invalid type given.",
        self::TOO_SHORT => "The input is less than %min% characters long.",
        self::TOO_LONG  => "The input is more than %max% characters long.",
    ];

}
