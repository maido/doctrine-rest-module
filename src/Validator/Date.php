<?php

namespace DoctrineRestModule\Validator;

class Date extends \Zend\Validator\Date
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID => "Invalid date.",
        self::INVALID_DATE => "Invalid input.",
        self::FALSEFORMAT => "Invalid date.",
    ];

}
