<?php

namespace DoctrineRestModule\Validator;

class ObjectExists extends \DoctrineModule\Validator\ObjectExists
{

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_NO_OBJECT_FOUND => "Item not found.",
    );

}
