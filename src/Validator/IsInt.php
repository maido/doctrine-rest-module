<?php

namespace DoctrineRestModule\Validator;

class IsInt extends \Zend\I18n\Validator\IsInt
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID => "Invalid type.",
        self::NOT_INT => "Invalid input.",
    ];

}
