<?php

namespace DoctrineRestModule\Validator;

class InArray extends \Zend\Validator\InArray
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_IN_ARRAY => 'Invalid input.',
    ];

}
