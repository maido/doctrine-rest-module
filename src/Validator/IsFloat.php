<?php

namespace DoctrineRestModule\Validator;

class IsFloat extends \Zend\I18n\Validator\IsFloat
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID => "Invalid type.",
        self::NOT_FLOAT => "Invalid input.",
    ];

}
