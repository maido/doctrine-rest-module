<?php

namespace DoctrineRestModule\Validator;

class UniqueObject extends \DoctrineModule\Validator\UniqueObject
{

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_OBJECT_NOT_UNIQUE => "There is already another item matching '%value%'.",
    );

}
