<?php

namespace DoctrineRestModule\Validator;

class NoObjectExists extends \DoctrineModule\Validator\NoObjectExists
{

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_OBJECT_FOUND => "An item matching '%value%' was found.",
    );

}
