<?php

namespace DoctrineRestModule\Service\Rest;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Doctrine\ORM\EntityManager;

class RestServiceFactory implements FactoryInterface
{

    /**
     * Create service
     * 
     * @param ServiceLocatorInterface $services
     * @return \DoctrineRestModule\Service\Rest\RestService
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $entityManager = $services->get(EntityManager::class);

        $config = $services->get('Config');
        $config = isset($config['doctrine-rest']) && is_array($config['doctrine-rest']) ?
                $config['doctrine-rest'] : [];

        $cache = $services->has('doctrine-rest-cache') ? $services->get('doctrine-rest-cache') : null;
        $files = $services->has('DoctrineFileModule\Service\File\FileService') ? $services->get('DoctrineFileModule\Service\File\FileService') : null;
        
        return new RestService($entityManager, $config, $cache, $files);
    }

}
