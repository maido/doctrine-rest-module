<?php

namespace DoctrineRestModule\Service\Rest;

interface RestServiceInterface
{

    /**
     * Get entity manager
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager();

    /**
     * Get config
     *
     * @return \DoctrineRestModule\Service\Rest\Config\ServiceConfigInterface
     */
    public function getConfig();
}
