<?php

namespace DoctrineRestModule\Service\Rest;

trait RestServiceAwareTrait
{

    /**
     * @var \DoctrineRestModule\Service\Rest\RestServiceInterface
     */
    protected $restService;

    /**
     * Set rest service
     *
     * @param \DoctrineRestModule\Service\Rest\RestServiceInterface $restService
     * @return self
     */
    public function setRestService(RestServiceInterface $restService)
    {
        $this->restService = $restService;
        return $this;
    }

    /**
     * Get rest service
     *
     * @return \DoctrineRestModule\Service\Rest\RestServiceInterface
     */
    public function getRestService()
    {
        return $this->restService;
    }

}
