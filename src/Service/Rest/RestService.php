<?php

namespace DoctrineRestModule\Service\Rest;

use Doctrine\ORM\EntityManager;
use DoctrineRestModule\Exception;
use Zend\Cache\Storage\Adapter\AbstractAdapter as CacheAdapter;
use DoctrineFileModule\Service\File\FileServiceInterface;

class RestService implements RestServiceInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $defaultRoutes = [
        'api' => 'doctrine-rest',
        'resource' => 'doctrine-rest/resource',
    ];

    /**
     * @var \Zend\Cache\Storage\Adapter\AbstractAdapter
     */
    protected $cache;

    /**
     * @var \DoctrineFileModule\Service\File\FileServiceInterface
     */
    protected $files;

    /**
     * Class constructor
     *
     * @param EntityManager $entityManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager, array $config, CacheAdapter $cache = null, FileServiceInterface $files = null)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
        $this->cache = $cache;
        $this->files = $files;
    }

    /**
     * Get entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Get config
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Get cache adapter
     *
     * @return \Zend\Cache\Storage\Adapter\AbstractAdapter
     */
    public function getCacheAdapter()
    {
        return $this->cache;
    }

    /**
     * Get file system
     * 
     * @return \DoctrineFileModule\Service\File\FileServiceInterface
     */
    public function getFileService()
    {
        return $this->files;
    }

    /**
     * Get routes
     *
     * @return array
     */
    public function getRoutes()
    {
        return isset($this->config['routes']) && is_array($this->config['routes']) ?
                $this->config['routes'] : $this->defaultRoutes;
    }

    /**
     * Get route
     *
     * @param string $name
     * @return string|null
     */
    public function getRoute($name)
    {
        $routes = $this->getRoutes();
        return isset($routes[$name]) ? $routes[$name] : null;
    }

    /**
     * Get resources
     *
     * @return array
     */
    public function getResources()
    {
        return isset($this->config['resources']) && is_array($this->config['resources']) ?
                $this->config['resources'] : [];
    }

    /**
     * Get resource config
     *
     * @param name $name
     * @return array|null
     */
    protected function getResourceConfig($name)
    {
        $resources = $this->getResources();

        if (isset($resources[$name]['class'])) {
            $config = $resources[$name];
            $config['name'] = $name;
        } elseif (isset($resources[$name]) && is_string($resources[$name])) {
            $config = [
                'class' => $resources[$name],
                'name' => $name,
            ];
        } else {
            $class = $name;
            foreach ($resources as $name => $config) {
                if (isset($config['class']) && $config['class'] === $class) {
                    $config['name'] = $name;
                    break;
                } elseif ($config === $class) {
                    $config = [
                        'class' => $config,
                        'name' => $name,
                    ];
                    break;
                } else {
                    unset($config);
                }
            }
        }

        if (isset($config)) {
            if (!isset($config['resource_title'])) {
                $filter = new \Zend\Filter\Word\CamelCaseToSeparator(' ');
                $parts = explode('\\', $config['class']);
                $config['resource_title'] = $filter->filter(end($parts));
            }

            if (!isset($config['collection_title'])) {
                $inflector = new \Doctrine\Common\Inflector\Inflector();
                $config['collection_title'] = $inflector->pluralize($config['resource_title']);
            }

            if (!isset($config['route'])) {
                $config['route'] = $this->getRoute('resource');
            }

            if (isset($this->config['default_items_per_page']) && $this->config['default_items_per_page'] > 0) {
                $config['default_items_per_page'] = $this->config['default_items_per_page'];
            }

            return $config;
        }

        return null;
    }

    /**
     * Get resource
     *
     * @param string $name
     * @param array $options
     * @param bool $isCollection
     * @return \DoctrineRestModule\Rest\AbstractResource
     * @throws Exception\DomainException
     */
    public function getResource($name, array $options = [], $isCollection = false)
    {
        if (is_subclass_of($name, \Doctrine\ORM\Proxy\Proxy::class)) {
            $name = get_parent_class($name);
        }

        if (null === $config = $this->getResourceConfig($name)) {
            $names = implode(', ', array_keys($this->getResources()));
            $message = sprintf('Undefined resource with name "%s"; please try one of "%s" instead.', $name, $names);
            throw new Exception\DomainException($message, 404);
        }

        if ($isCollection) {
            $resourceClass = isset($this->config['collection_class']) ?
                    $this->config['collection_class'] :
                    \DoctrineRestModule\Rest\HalCollection::class;
        } else {
            $resourceClass = isset($this->config['resource_class']) ?
                    $this->config['resource_class'] :
                    \DoctrineRestModule\Rest\HalResource::class;
        }

        if (isset($options['request'])) {
            $request = $options['request'];
            unset($options['request']);
        } else {
            $request = null;
        }

        return new $resourceClass($config, $options, $this, $request);
    }

    /**
     * Get input filter
     *
     * @param string $class
     * @param bool $includeIdentifier
     * @return \Zend\InputFilter\InputFilterInterface
     */
    public function getInputFilter($class, $includeIdentifier = true, array $patches = null)
    {
        $config = $this->getResourceConfig($class);
        $metadata = $this->getEntityManager()->getClassMetadata($config['class']);
        $spec = [];

        foreach ($metadata->getFieldNames() as $field) {
            $mapping = $metadata->getFieldMapping($field);

            if (isset($mapping['id']) && (!$includeIdentifier || $metadata->idGenerator instanceof \Doctrine\ORM\Id\IdentityGenerator)) {
                // exclude identifier
                continue;
            }

            $name = $mapping['columnName'];

            if (is_array($patches) && !in_array($name, $patches)) {
                continue;
            }

            $spec[$name] = $this->getFieldSpec($mapping, $metadata->getName());
        }

        foreach ($metadata->getAssociationNames() as $association) {
            $mapping = $metadata->getAssociationMapping($association);

            if ($mapping['type'] !== 1 && $mapping['type'] !== 2) {
                continue;
            }

            $name = $mapping['fieldName'];

            if (is_array($patches) && !in_array($name, $patches)) {
                continue;
            }

            $spec[$name] = $this->getAssociationSpec($mapping, $metadata->getName());
        }

        if (isset($config['input_filter']) && is_array($config['input_filter'])) {
            $spec = array_merge($spec, $config['input_filter']);
        }
        
        $factory = new \Zend\InputFilter\Factory();

        return $factory->createInputFilter($spec);
    }

    /**
     * Get field spec
     *
     * @param array $mapping
     * @param string $entity
     * @return array
     */
    protected function getFieldSpec(array $mapping, $entity)
    {
        $spec = [
            'name' => $mapping['columnName'],
            'required' => !$mapping['nullable'],
            'allow_empty' => $mapping['nullable'],
            'filters' => [
                ['name' => \Zend\Filter\StripTags::class],
                ['name' => \Zend\Filter\StringTrim::class],
            ],
        ];

        if (isset($mapping['id']) && $mapping['id']) {
            $repository = $this->getEntityManager()->getRepository($entity);
            $spec['validators'][] = [
                'name' => \DoctrineRestModule\Validator\NoObjectExists::class,
                'options' => [
                    'object_repository' => $repository,
                    'fields' => [$mapping['fieldName']],
                ],
            ];
        }

        if (isset($mapping['unique']) && $mapping['unique']) {
            $repository = $this->getEntityManager()->getRepository($entity);
            $spec['validators'][] = [
                'name' => \DoctrineRestModule\Validator\UniqueObject::class,
                'options' => [
                    'object_repository' => $repository,
                    'fields' => [$mapping['fieldName']],
                    'object_manager' => $this->getEntityManager(),
                    'use_context' => true,
                ],
            ];
        }

        switch ($mapping['type']) {
            case 'smallint' :
            case 'integer' :
            case 'bigint' :
                $spec['validators'][] = ['name' => \DoctrineRestModule\Validator\IsInt::class];
                break;
            case 'decimal' :
            case 'float' :
                $spec['validators'][] = ['name' => \DoctrineRestModule\Validator\IsFloat::class];
                break;
            case 'date' :
                $spec['validators'][] = [
                    'name' => \DoctrineRestModule\Validator\Date::class,
                    'options' => ['format' => 'Y-m-d'],
                ];
                break;
            case 'datetime' :
                $spec['validators'][] = [
                    'name' => \DoctrineRestModule\Validator\Date::class,
                    'options' => ['format' => 'Y-m-d H:i:s'],
                ];
                break;
        }

        return $spec;
    }

    /**
     * Get association spec
     *
     * @param array $mapping
     * @param string $entity
     * @return array
     */
    protected function getAssociationSpec(array $mapping, $entity)
    {
        $required = isset($mapping['joinColumns'][0]['nullable']) ?
                !$mapping['joinColumns'][0]['nullable'] : false;

        // we just need IDs, so use custom query to raise performance
        $dql = sprintf('SELECT e.id FROM %s e', $mapping['targetEntity']);
        $haystack = $this->getEntityManager()->createQuery($dql)
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $spec = [
            'name' => $mapping['fieldName'],
            'required' => $required,
            'allow_empty' => !$required,
            'validators' => [
                [
                    'name' => \DoctrineRestModule\Validator\InArray::class,
                    'options' => [
                        'haystack' => $haystack,
                        'recursive' => true
                    ],
                ],
            ],
        ];

        // 1:1 associations require unique values
        if ($mapping['type'] === 1) {
            $repository = $this->getEntityManager()->getRepository($entity);
            $spec['validators'][] = [
                'name' => \DoctrineRestModule\Validator\UniqueObject::class,
                'options' => [
                    'object_repository' => $repository,
                    'fields' => [$mapping['fieldName']],
                    'object_manager' => $this->getEntityManager(),
                    'use_context' => true,
                ],
            ];
        }

        return $spec;
    }

}
