<?php

namespace DoctrineRestModule\Service\Log;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class LoggerFactory implements FactoryInterface
{

    /**
     * Create service
     * 
     * @param ServiceLocatorInterface $services
     * @return \DoctrineRestModule\Service\Rest\RestService
     */
    public function createService(ServiceLocatorInterface $services)
    {
        if (!is_dir('data/log')) {
            mkdir('data/log');
        }

        $writer = new Stream('data/log/rest.log');
        $logger = new Logger();
        $logger->addWriter($writer);

        return $logger;
    }

}
