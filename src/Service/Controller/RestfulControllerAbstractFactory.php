<?php

namespace DoctrineRestModule\Service\Controller;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use DoctrineRestModule\Controller\AbstractRestfulController;
use DoctrineRestModule\Service\Rest\RestService;
use Zend\Log\Logger;

class RestfulControllerAbstractFactory implements AbstractFactoryInterface
{

    /**
     * Determine if we can create a service with name
     *
     * @param ServiceLocatorInterface $controllers
     * @param string $name
     * @param string $requestedName
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $controllers, $name, $requestedName)
    {
        return class_exists($requestedName) && is_subclass_of($requestedName, AbstractRestfulController::class);
    }

    /**
     * Create service with name
     *
     * @param ServiceLocatorInterface $controllers
     * @param string $name
     * @param string $requestedName
     * @return \DoctrineRestModule\Controller\AbstractRestfulController
     */
    public function createServiceWithName(ServiceLocatorInterface $controllers, $name, $requestedName)
    {
        $locator = $controllers->getServiceLocator();
        $restService = $locator->get(RestService::class);
        $logger = $locator->get(Logger::class);

        $controller = new $requestedName($restService, $logger);

        return $controller;
    }

}
