<?php

namespace DoctrineRestModule;

use Zend\Mvc\Router\Http;

return [
    'doctrine-rest' => include __DIR__ . '/rest.global.php',
    'caches' => [
    /**
     * Add a cache named 'doctrine-rest-cache'
     */
    //'doctrine-rest-cache' => [
    //   // your cache config options...
    //],
    ],
    'router' => [
        'routes' => [
            'doctrine-rest' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/api',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'resource' => [
                        'type' => Http\Segment::class,
                        'options' => [
                            'route' => '/:name[/:id]',
                            'constraints' => [
                                'name' => '[a-z0-9\-]+',
                                'id' => '[a-z0-9\-]+',
                            ],
                            'defaults' => [
                                'controller' => Controller\ResourceController::class,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\Rest\RestService::class => Service\Rest\RestServiceFactory::class,
            \Zend\Log\Logger::class => Service\Log\LoggerFactory::class,
        ],
    ],
    'controllers' => [
        'abstract_factories' => [
            Service\Controller\RestfulControllerAbstractFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'collection' => View\Helper\Collection::class,
            'resource' => View\Helper\Resource::class,
            'title' => View\Helper\Title::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
        // @todo
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
