<?php

return [
    /**
     * Enable bearer token requirement
     */
    'require_bearer' => false,
    /**
     * Uncomment if this API requires a bearer token
     */
    //'bearer_token' => 'abcdef123456',
    //'protected_route' => 'doctrine-rest',
    /**
     * Custom configuration for resources
     */
    'resources' => [],
];